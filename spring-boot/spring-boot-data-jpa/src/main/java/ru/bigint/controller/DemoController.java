package ru.bigint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.bigint.dao.PersonDao;
import ru.bigint.model.Person;

import java.util.List;
import java.util.Random;

@RestController
public class DemoController {

    @Autowired
    PersonDao personDao;

    @GetMapping("/generate/")
    public Person generate() {
        String[] names = {"Alex", "John", "Donald", "Mike", "Olga"};
        Random random = new Random();

        Person person = new Person(names[random.nextInt(5)], random.nextInt(100));
        person = personDao.save(person);

        return person;
    }


    @GetMapping("/get/")
    public Person get(@RequestParam(value = "id") int id) {
        Person person = personDao.findById(id).get();
        return person;
    }


    @GetMapping("/list/")
    public List<Person> list() {
        return personDao.findAll(Sort.by("age"));
    }

}
