package ru.bigint.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.bigint.model.Person;

@Repository
public interface PersonDao extends JpaRepository<Person, Integer> {
}
