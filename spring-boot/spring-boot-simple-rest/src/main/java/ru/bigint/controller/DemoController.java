package ru.bigint.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.bigint.model.Data;

@RestController
public class DemoController {

    private int counter;

    @GetMapping("/")
    public Data restExampleRequest(@RequestParam(value = "name", defaultValue = "User") String name) {
        counter++;
        Data data = new Data(counter, name);
        return data;
    }

}
