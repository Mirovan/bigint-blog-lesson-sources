package ru.bigint.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class DemoController {

    @GetMapping("/")
    public ModelAndView mainPage() {
        String[] list = {"Cat", "Dog", "Mouse", "Fish", "Elephant"};

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("list", list);
        modelAndView.setViewName("index");

        return modelAndView;
    }

}
