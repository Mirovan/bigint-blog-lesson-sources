package ru.bigint.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.bigint.model.Person;

import java.util.ArrayList;
import java.util.List;

@Controller
public class DemoController {
    List<Person> list = new ArrayList<>();

    @GetMapping("/list/")
    public ModelAndView list() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("list", list);
        modelAndView.setViewName("list");

        return modelAndView;
    }

    @GetMapping("/add/")
    public ModelAndView add() {
        Person person = new Person();

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("person", person);
        modelAndView.setViewName("add");

        return modelAndView;
    }

    @RequestMapping(value = "/add/", method = RequestMethod.POST)
    public ModelAndView addPost(@ModelAttribute Person person) {
        list.add(person);

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.addObject("person", person);
        modelAndView.setViewName("add-complite");

        return modelAndView;
    }

}
