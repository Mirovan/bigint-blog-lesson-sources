package ru.bigint.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import ru.bigint.model.Person;
import ru.bigint.util.PersonMapper;

import java.util.List;
import java.util.Random;

@RestController
public class DemoController {

    @Autowired
    JdbcTemplate jdbcTemplate;

    private String[] names = {"Alex", "Bob", "John", "Max", "Nick"};

    @GetMapping("/generate/")
    public Person generate() {
        Random random = new Random();

        Person person = new Person(names[random.nextInt(5)], random.nextInt(100));
        String query = "INSERT INTO person(name, age) VALUES(?, ?)";
        jdbcTemplate.update(query, person.getName(), person.getAge());

        return person;
    }


    @GetMapping("/get/")
    public Person get(@RequestParam(value = "id") int id) {
        String query = "SELECT * FROM person WHERE id = ?";
        Person person = jdbcTemplate.queryForObject(query, new Object[]{id}, new PersonMapper());
        return person;
    }


    @GetMapping("/list/")
    public List<Person> list() {
        String query = "SELECT * FROM person";
        List<Person> persons = jdbcTemplate.query(query, new PersonMapper());
        return persons;
    }

}
