package ru.bigint;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class DemoController {

    @GetMapping("/")
    public String mainPage(Model model, @RequestParam(value = "name", defaultValue = "user") String name) {
        model.addAttribute("name", name);
        return "index";
    }

}
