# Springboot examples (bigint.ru)

- [spring-boot-simple](spring-boot-simple) - Простой пример использования Spring Boot
- [spring-boot-simple-rest](spring-boot-simple-rest) - Простой пример использования REST со Spring Boot
- [spring-boot-jsp](spring-boot-jsp) - Пример использования JSP со Spring Boot
- [spring-boot-jdbc](spring-boot-jdbc) - Пример использования приложения Spring Boot и базы данных Postgres с помощью JdbcTemplate   
- [spring-boot-data-jpa](spring-boot-data-jpa) - Использование JPA и Spring Data в Spring Boot приложении
- [spring-boot-thymeleaf](spring-boot-thymeleaf) - Использование шаблонизатора Thymeleaf для Spring Boot приложения   
- [spring-boot-thymeleaf-form](spring-boot-thymeleaf-form) - Обработка форм в шаблонизаторе Thymeleaf для Spring Boot приложения

