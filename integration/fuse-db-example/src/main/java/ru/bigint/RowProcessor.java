package ru.bigint;

import org.apache.camel.Exchange;
import org.apache.camel.Processor;

import java.util.HashMap;
import java.util.Map;

public class RowProcessor implements Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        Map<String, Object> petRow = exchange.getIn().getBody(Map.class);
        Pet pet = new Pet();
        pet.setId((Integer) petRow.get("id"));
        pet.setName((String) petRow.get("name"));
        pet.setAge((Integer) petRow.get("age"));

        StringBuilder st = new StringBuilder();
        st.append("<pet>");
        st.append("<id>" + pet.getId() + "</id>");
        st.append("<name>" + pet.getName() + "</name>");
        st.append("<age>" + pet.getAge() + "</age>");
        st.append("</pet>");

        Map<String, Object> rowXML = new HashMap<>();
        rowXML.put("content", st.toString());

        exchange.getOut().setBody(rowXML);
    }
}
