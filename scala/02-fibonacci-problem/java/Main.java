public class Main {

    public static void main(String[] args) {
        int res = fibonacci(40);
        System.out.println(res);

        res = fibonacciRecoursive(40);
        System.out.println(res);
    }


    private static int fibonacci(int n) {
        int res = 1;

        if (n > 2) {
            int prev = 1;

            for (int i = 2; i < n; i++) {
                int temp = res;
                res = res + prev;
                prev = temp;
            }
        }

        return res;
    }


    private static int fibonacciRecoursive(int n) {
        if (n == 1 || n == 2) return 1;
        return fibonacciRecoursive(n-1) + fibonacciRecoursive(n-2);
    }

}
