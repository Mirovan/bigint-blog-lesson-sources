
object Main extends App {

  def fibonacci(n: Int): Int = {
    if (n == 1 || n == 2 )
      1
    else
      fibonacci(n - 1) + fibonacci(n - 2)
  }


  def fibonacciPerfect(n: Int): Int = {
    def fibonacciInc(prev: Int, current: Int, n: Int): Int ={
      if (n <= 2)
        current
      else {
        fibonacciInc(current, current+prev, n-1)
      }
    }

    fibonacciInc(1, 1, n)
  }



  println(fibonacciPerfect(7))
}
