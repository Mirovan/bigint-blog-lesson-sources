public class Main {

    public static void main(String[] args) {
        int res = factorial(4);
        System.out.println(res);
    }

    private static int factorial(int n) {
        if (n == 1) return 1;
        return n * factorial(n-1);
    }

}
