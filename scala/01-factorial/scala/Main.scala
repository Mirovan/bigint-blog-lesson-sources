object Main extends App {

  def factorial(n: Int): Int = {
    if (n == 1)
      1
    else
      n * factorial(n-1)
  }


  def factorialPerfect(n: Int): Int = {
    def factorialHelper(k: Int, accumulator: Int): Int = {
      if (k == 1)
        accumulator
      else {
        println(k + " " + accumulator)
        factorialHelper(k-1, k * accumulator)
      }

    }

    factorialHelper(n, 1)
  }


//  println( factorial(5) )
//    println( factorial(5) ) //StackOverFlow
  println( factorialPerfect(5) )

}
