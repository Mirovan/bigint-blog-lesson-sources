public class Main {

    public static void main(String[] args) {
        boolean res = isPrime(23);
//        boolean res = isPrimePerfect(23);
        System.out.println(res);
    }

    private static boolean isPrime(int n) {
        if (n == 1) return false;
        for (int i=2; i<=n/2; i++) {
            if (n % i == 0) return false;
        }

        return true;
    }

    private static boolean isPrimePerfect(int n) {
        if (n == 1) return false;
        if (n != 2 && n % 2 == 0) return false;

        int k = (int) Math.sqrt(n);
        for (int i=3; i<=k; i+=2) {
            if (n % i == 0) return false;
        }

        return true;
    }

}
