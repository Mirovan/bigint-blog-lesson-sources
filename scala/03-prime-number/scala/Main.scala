object Main extends App {

  def isPrime(n: Int): Boolean = {
    def isDivide(k: Int): Boolean = {
      if (k == 1)
        true
      else
        (n % k != 0) && isDivide(k-1)
    }

    if (n == 1)
      false
    else
      isDivide(n-1)
  }

  println( isPrime(23) )

}
