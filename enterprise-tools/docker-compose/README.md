# Использование docker-compose

## About
Приложение из 3 частей
- Запуск контейнера с Postgres
- Запуск контейнера c миграцией через Liquibase
- Сборка через maven и запуск контейнера c Spring Boot приложением


## Сборка
- docker-compose build
- docker-compose up